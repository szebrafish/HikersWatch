package com.zebra.hikerswatch;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


// import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends AppCompatActivity {

    TextView textViewInformation;

    LocationManager locationManager;
    LocationListener locationListener;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if /* Location permission was granted successfully */ (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //Set to only update GPS if distance changes over 5m
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 5, locationListener);
                setTextViewInformation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

            }
        }
    }

    public void setTextViewInformation(Location location){

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

        String latitude = Double.toString(location.getLatitude());
        String longitude = Double.toString(location.getLongitude());
        String accuracy = Double.toString(location.getAccuracy());
        String altitude = Double.toString(location.getAltitude());

        String addressLine1 = "No address found";
        String addressLine2 = "";
        String addressLine3 = "";

        try {

            //Gets address from GPS coordinates - only requests 1 result (still saves as List with single entry)
            List<Address> listAddresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            //Sets address string (or blanks it out if nothing found - will cause no marker title banner and prevent toast message)
            if (listAddresses != null && listAddresses.size() > 0){

                addressLine1 = listAddresses.get(0).getAddressLine(0);
                addressLine2 = listAddresses.get(0).getAddressLine(1);
                addressLine3 = listAddresses.get(0).getAddressLine(2);
            }
            else {

                addressLine1 = "No address found";
                addressLine2 = "";
                addressLine3 = "";
            }

        } catch (IOException e) {
            e.printStackTrace();

        }

        textViewInformation.setText("Latitude: " + latitude + "\r\n\r\nLongitude: " + longitude + "\r\n\r\nAccuracy: " + accuracy + "m\r\n\r\nAltitude: " + altitude + "m\r\n\r\nAddress:\r\n" + addressLine1 + "\r\n" + addressLine2 + "\r\n" + addressLine3);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewInformation = (TextView) findViewById(R.id.textViewInformation);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {

                setTextViewInformation(location);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if /* System is pre-MM (permission not required) */ (Build.VERSION.SDK_INT < 23) {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 5, locationListener);

        } else {

            if /* Permission not previously granted */ (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else  /* ^^ Permission already granted */ {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 5, locationListener);
                setTextViewInformation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

            }
        }
    }
}
